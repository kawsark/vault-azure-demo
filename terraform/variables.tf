variable "resource_group" {
  description = "The name of the Resource group"
  default = "rg-alice-demoapp-dev"
}

variable "storage_acc_name" {
  default = "saalicedemoappdev"
}

variable "location" {
  description = "The Azure Region in which the resources in this example should exist"
  default = "West US"
}

variable "tags" {
  type        = map
  default     = {
    env="dev"
    app="demoapp"
    owner="alice"
  }
  description = "Any tags which should be assigned to the resources in this example"
}
