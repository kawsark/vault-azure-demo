#!/bin/sh

echo "INFO: Creating Resource Group level Role: ${RESOURCE_GROUP}-role"
vault write ${AZ_SECRET_PATH}/roles/${RESOURCE_GROUP}-role ttl=5m azure_roles=-<<EOF
[ {
    "role_name": "Contributor",
    "scope":  "/subscriptions/${ARM_SUBSCRIPTION_ID}/resourceGroups/${RESOURCE_GROUP}"
	}]
EOF

echo "INFO: Reading Role: ${RESOURCE_GROUP}-role"
vault read ${AZ_SECRET_PATH}/roles/${RESOURCE_GROUP}-role

echo "INFO: Creating Subscription level Role: subscription-role"
vault write ${AZ_SECRET_PATH}/roles/subscription-role ttl=5m azure_roles=-<<EOF
[ {
    "role_name": "Contributor",
    "scope":  "/subscriptions/${ARM_SUBSCRIPTION_ID}"
        }]
EOF

echo "INFO: Reading Role: $subscription-role"
vault read ${AZ_SECRET_PATH}/roles/subscription-role
