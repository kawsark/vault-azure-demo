## Setting up GitLab Runners on Kubernetes
Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

### Install helm charts
Please install Helm V3 using the steps: [Install Helm for your system](https://helm.sh/docs/using_helm/#install-helm)


### Installing the GitLab Runner helm chart
```
cd helm
export RUNNER_TOKEN=<runner-registration-token from GitLab CI>
make install_runner
```

### Check pods deployed
```
kubectl get pods -n gitlab
kubectl logs -f <podname> -n gitlab
```

### Uninstalling
```
make clean
```

Reference: [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)
Note: if you are getting RBAC issues during helm deployment, please consult this excellent guide [Configure RBAC for Helm](https://docs.bitnami.com/kubernetes/how-to/configure-rbac-in-your-kubernetes-cluster/#use-case-2-enable-helm-in-your-cluster).

