azure_info:
	az account list
	echo "Please run export ARM_SUBSCRIPTION_ID=<desired_subscription_id>"

1_create_vault_demo:
	@az ad sp create-for-rbac -n "Kawsar-Vault-Demo" --role Owner --scopes "/subscriptions/${ARM_SUBSCRIPTION_ID}"

1_validate:
	scripts/validate_vars.sh

2_assign_permissions:
	scripts/assign_permissions.sh

3_azure_secrets_engine:
	vault secrets enable -path=${AZ_SECRET_PATH} azure
	vault write ${AZ_SECRET_PATH}/config \
	  subscription_id=${ARM_SUBSCRIPTION_ID} \
	  tenant_id=${ARM_TENANT_ID} \
	  client_id=${ARM_CLIENT_ID} \
	  client_secret=${ARM_CLIENT_SECRET}

4_create_resource_group:
	az group create -l westus -n "${RESOURCE_GROUP}"

4_azure_role:
	scripts/create_role.sh

5_cron_job_cred:
	echo "Creating credential with name cron-job-demo"
	az ad sp create-for-rbac -n "cron-job-demo" --scopes /subscriptions/${ARM_SUBSCRIPTION_ID} > cron-job-demo.json
	echo "Looking up Object ID:"
	az ad app list --filter "displayname eq 'cron-job-demo'" | jq -r '.[].objectId'

5_cron_job_role:
	vault write ${AZ_SECRET_PATH}/roles/cron-job-role ttl=25h application_object_id=${OBJECT_ID}

6_rotate_demo:
	scripts/rotate_demo.sh

7_create_tf_policy:
	scripts/create_tf_policy.sh

show_leases:
	curl -s --header "X-Vault-Token: ${VAULT_TOKEN}" --request LIST "${VAULT_ADDR}/v1/sys/leases/lookup/${AZ_SECRET_PATH}/creds/${RESOURCE_GROUP}-role" | jq .

clean: 
	rm -f *.json roleid secretid
	az ad sp delete --id 'http://Kawsar-Vault-Demo'
	az group delete -n ${RESOURCE_GROUP}
	az ad sp delete --id 'http://cron-job-demo'
