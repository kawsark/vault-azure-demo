resource "vault_jwt_auth_backend" "gitlab-jwt" {
  description  = "JWT Auth method for GitLab runner"
  path         = "jwt"
  jwks_url     = "https://gitlab.com/-/jwks"
  bound_issuer = "gitlab.com"
}

resource "vault_jwt_auth_backend_role" "gitlab-jwt-role" {
  backend        = vault_jwt_auth_backend.gitlab-jwt.path
  role_name      = "gitlab"
  token_policies = ["gitlab-runner"]
  bound_claims = {
    project_id = var.gitlab-project-id
    ref_type   = "branch"
    ref        = var.gitlab-project-branch
  }
  user_claim             = "user_email"
  role_type              = "jwt"
  token_explicit_max_ttl = 300
}