resource "vault_auth_backend" "approle" {
  type = "approle"
}

resource "vault_approle_auth_backend_role" "gitlab" {
  backend        = vault_auth_backend.approle.path
  role_name      = "gitlab"
  token_policies = ["gitlab-runner"]
}

resource "vault_approle_auth_backend_role_secret_id" "id" {
  backend   = vault_auth_backend.approle.path
  role_name = vault_approle_auth_backend_role.gitlab.role_name
}

resource "vault_approle_auth_backend_login" "login" {
  backend   = vault_auth_backend.approle.path
  role_id   = vault_approle_auth_backend_role.gitlab.role_id
  secret_id = vault_approle_auth_backend_role_secret_id.id.secret_id
}