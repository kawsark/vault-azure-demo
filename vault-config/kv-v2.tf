resource "vault_mount" "kv2" {
  path        = "kv2"
  type        = "kv-v2"
  description = "KV v2 engine mount"
}

resource "vault_generic_secret" "tfc_token" {
  path      = "${vault_mount.kv2.path}/tfc_token"
  data_json = <<EOT
{
  "token":   "${var.tfc_token}"
}
EOT
}