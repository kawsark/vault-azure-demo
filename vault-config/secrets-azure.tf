resource "vault_azure_secret_backend" "azure" {
  use_microsoft_graph_api = true
  path                    = var.az-secret-path
  subscription_id         = var.ARM_SUBSCRIPTION_ID
  tenant_id               = var.ARM_TENANT_ID
  client_id               = var.ARM_CLIENT_ID
  client_secret           = var.ARM_CLIENT_SECRET
  environment             = "AzurePublicCloud"
}

resource "vault_azure_secret_backend_role" "rg_role" {
  backend = vault_azure_secret_backend.azure.path
  role    = "${var.az-resource-group}-role"
  ttl     = 300
  max_ttl = 600

  azure_roles {
    role_name = "Contributor"
    scope     = "/subscriptions/${var.ARM_SUBSCRIPTION_ID}/resourceGroups/${var.az-resource-group}"
  }
}

resource "vault_azure_secret_backend_role" "subcription_role" {
  backend = vault_azure_secret_backend.azure.path
  role    = "subscription-role"
  ttl     = 300
  max_ttl = 600

  azure_roles {
    role_name = "Contributor"
    scope     = "/subscriptions/${var.ARM_SUBSCRIPTION_ID}"
  }
}

resource "vault_azure_secret_backend_role" "description_role" {
  backend = vault_azure_secret_backend.azure.path
  role    = "developer-role"
  ttl     = 300
  max_ttl = 600

  azure_roles {
    role_name = "Contributor"
    scope     = "/subscriptions/${var.ARM_SUBSCRIPTION_ID}"
  }
}