# Required variables
variable "gitlab-project-id" {
  description = "The project ID for Gitlab to associate with JWT Auth method"
}

variable "tfc_token" {
  description = "The TFC token for the CICD runner"
}

variable "ARM_SUBSCRIPTION_ID" {
  description = "Azure subscription ID"
}

variable "ARM_TENANT_ID" {
  description = "Azure Tenant ID"
}

variable "ARM_CLIENT_ID" {
  description = "Azure Client ID"
}

variable "ARM_CLIENT_SECRET" {
  description = "Azure Client secret"
}

# Optional variables
variable "top-ns" {
  description = "The root namespace. For HCP it is admin. For regular vault leave empty"
  default     = "admin"
}

variable "az-secret-path" {
  description = "The path to mount the AZ secrets engine"
  default     = "azure-demo"
}

variable "az-resource-group" {
  description = "a resource group associated with Azure dynamic creds"
  default     = "rg-alice-demoapp-dev"
}

variable "gitlab-project-branch" {
  description = "The branch for the pipeline runner"
  default     = "master"
}
