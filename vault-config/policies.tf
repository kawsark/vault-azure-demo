# Create an admin policy in the admin namespace
resource "vault_policy" "admin_policy" {
  name     = "admins"
  policy   = file("admin-policy.hcl")
}


# Create a policy for a CICD tool to access secrets
resource "vault_policy" "cicd_policy" {
  name = "dev-team"

  policy = <<EOT
path "kv2/data/tfc_token" {
  capabilities = ["read"]
}

path "${var.az-secret-path}/creds/${var.az-resource-group}-role" {
  capabilities = ["read"]
}

path "${var.az-secret-path}/creds/subscription-role" {
  capabilities = ["read"]
}

path "${var.az-secret-path}/creds/developer-role" {
  capabilities = ["read"]
}

EOT
}